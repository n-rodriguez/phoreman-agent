import unittest
import sys
from libpyomshell.pyomshell import omshell

class omshellTestCase(unittest.TestCase):

  def setUp(self):
    config_dhcp = {}
    config_dhcp['omshell_cmd'] = '/usr/bin/omshell'
    config_dhcp['server'] = '127.0.0.1'
    config_dhcp['port'] = '7911'
    config_dhcp['key_name'] = 'omapi_key'
    config_dhcp['key_secret'] = 'sN90+CmZ2EjrJIanr7laF8tJiDDXPxhHGljVc58Pgwl5M+p1A4c9Au59Nr0G2VjTaobPDQ0BUBB6B2dM4ljWQw=='
    config_dhcp['config_file'] = '/etc/dhcp/dhcpd.conf'
    config_dhcp['lease_file'] = '/var/lib/dhcp/dhcpd.leases'

    self.OS = omshell(config_dhcp)


  def test_isValidIpv4(self):
    bad_ip = (
                '192.',
                '192.168',
                '192.168.0.',
                '192.168.0.0.',
                '256.256.256.256'
              )

    good_ip = (
                '192.168.0.1',
                '192.168.0.1',
                '192.168.0.1'
              )

    print "\n\tFALSE CASES :"
    for ip_address in bad_ip:
      try:
        toto = self.OS._isValidIpv4(ip_address)
      except NameError:
        print "\t%s" % (ip_address)
        pass
      else:
        print "expected a NameError"

    print "\n\tTRUE CASES :"
    for ip_address in good_ip:
      toto = self.OS._isValidIpv4(ip_address)
      print "\t%s\t%s" % (ip_address, toto)
      self.assertEqual(toto, True)


  def test_isValidHostName(self):
    bad_hostname = (
                    'thetuxhouse',
                    'thetuxhouse.',
                    'thetuxhouse.priv',
                    'thetuxhouse.priv.',
                    '.thetuxhouse.priv',
                    '.thetuxhouse.priv.',
                  )

    good_hostname = (
                    'toto.thetuxhouse.priv',
                    'toto.thetuxhouse.priv.',
                    'toto-1.thetuxhouse.priv.',
                    '1-toto-1.thetuxhouse.priv.',
                  )

    print "\n\tFALSE CASES :"
    for hostname in bad_hostname:
      try:
        toto = self.OS._isValidHostName(hostname)
      except NameError:
        print "\t%s" % (hostname)
        pass
      else:
        print "expected a NameError"

    print "\n\tTRUE CASES :"
    for hostname in good_hostname:
      toto = self.OS._isValidHostName(hostname)
      print "\t%s\t%s" % (hostname, toto)
      self.assertEqual(toto, True)


  def test_isValidMacAddress(self):
    bad_mac_address = (
                    '74',
                    '74:',
                    '74:2',
                    '74:2f',
                    '74:2f:',
                    '74:2f:6',
                    '74:2f:68',
                    '74:2f:68:',
                    '74:2f:68:a',
                    '74:2f:68:ae',
                    '74:2f:68:ae:',
                    '74:2f:68:ae:c',
                    '74:2f:68:ae:cf',
                    '74:2f:68:ae:cf:',
                    '74:2f:68:ae:cf:c',
                    '74:2f:68:ae:cf:cZ',
                    '74:2f:68:ae:cf:GG',
                    '74:Zf:168:ae:cf:GG'
                  )

    print "\n\tFALSE CASES :"
    for mac_address in bad_mac_address:
      try:
        toto = self.OS._isValidMacAddress(mac_address)
      except NameError:
        print "\t%s" % (mac_address)
        pass
      else:
        print "expected a NameError"


  def test_lookupByMac(self):
    bad_mac_address = (
                'de:ad:63:04:ad:06',
              )

    good_mac_address = {
                'test1': {'mac': 'de:ad:63:04:ad:05', 'return': {'host_type': 'host', 'host_name': 'brain1.thetuxhouse.priv', 'host_mac': 'de:ad:63:04:ad:05', 'host_ip': '192.168.0.101'}},
                'test2': {'mac': '5a:d1:e7:14:bd:86', 'return': {'host_type': 'host', 'host_name': 'brain2.thetuxhouse.priv', 'host_mac': '5a:d1:e7:14:bd:86', 'host_ip': '192.168.0.102'}},
              }

    print "\n\tFALSE CASES :"
    for mac_address in bad_mac_address:
      toto = self.OS.lookupByMac(mac_address)
      print "\t%s\t%s" % (mac_address, toto)
      self.assertEqual(toto, False)

    print "\n\tTRUE CASES :"
    for tests, value in good_mac_address.iteritems():
      toto = self.OS.lookupByMac(value['mac'])
      print "\t%s" % (toto)
      self.assertEqual(toto, value['return'])


  def test_lookupByIp(self):
    bad_ip_address = (
                '192.168.0.253',
              )

    good_ip_address = {
                'test1': {'ip': '192.168.0.101', 'return': {'host_type': 'host', 'host_name': 'brain1.thetuxhouse.priv', 'host_mac': 'de:ad:63:04:ad:05', 'host_ip': '192.168.0.101'}},
                'test2': {'ip': '192.168.0.102', 'return': {'host_type': 'host', 'host_name': 'brain2.thetuxhouse.priv', 'host_mac': '5a:d1:e7:14:bd:86', 'host_ip': '192.168.0.102'}},
              }

    print "\n\tFALSE CASES :"
    for ip_address in bad_ip_address:
      toto = self.OS.lookupByIp(ip_address)
      print "\t%s\t%s" % (ip_address, toto)
      self.assertEqual(toto, False)

    print "\n\tTRUE CASES :"
    for tests, value in good_ip_address.iteritems():
      toto = self.OS.lookupByIp(value['ip'])
      print "\t%s" % (toto)
      self.assertEqual(toto, value['return'])


  def test_lookupByHostName(self):
    bad_hostname = (
                'brain3.thetuxhouse.priv',
              )

    good_hostname = {
                'test1': {'hostname': 'brain1.thetuxhouse.priv', 'return': {'host_type': 'host', 'host_name': 'brain1.thetuxhouse.priv', 'host_mac': 'de:ad:63:04:ad:05', 'host_ip': '192.168.0.101'}},
                'test2': {'hostname': 'brain2.thetuxhouse.priv', 'return': {'host_type': 'host', 'host_name': 'brain2.thetuxhouse.priv', 'host_mac': '5a:d1:e7:14:bd:86', 'host_ip': '192.168.0.102'}},
              }

    print "\n\tFALSE CASES :"
    for hostname in bad_hostname:
      toto = self.OS.lookupByHostName(hostname)
      print "\t%s\t%s" % (hostname, toto)
      self.assertEqual(toto, False)

    print "\n\tTRUE CASES :"
    for tests, value in good_hostname.iteritems():
      toto = self.OS.lookupByHostName(value['hostname'])
      print "\t%s" % (toto)
      self.assertEqual(toto, value['return'])


suite = unittest.TestLoader().loadTestsFromTestCase(omshellTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)
