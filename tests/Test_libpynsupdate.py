import unittest
import sys
from libpynsupdate.pynsupdate import nsupdate

class nsupdateTestCase(unittest.TestCase):

  def setUp(self):
    config_dns = {}
    config_dns['nsupdate_cmd'] = '/usr/bin/nsupdate'
    config_dns['server'] = '127.0.0.1'
    config_dns['key_file'] = '/etc/bind/Kthetuxhouse.priv.+157+22812.key'

    self.NS = nsupdate(config_dns)


  def test_isValidIpv4(self):
    bad_ip = (
                '192.',
                '192.168',
                '192.168.0.',
                '192.168.0.0.',
                '256.256.256.256',
              )

    good_ip = (
                '192.168.0.1',
                '192.168.0.1',
                '192.168.0.1',
              )

    print "\n\tFALSE CASES :"
    for ip_address in bad_ip:
      try:
        toto = self.NS._isValidIpv4(ip_address)
      except NameError:
        print "\t%s" % (ip_address)
        pass
      else:
        print "expected a NameError"

    print "\n\tTRUE CASES :"
    for ip_address in good_ip:
      toto = self.NS._isValidIpv4(ip_address)
      print "\t%s\t%s" % (ip_address, toto)
      self.assertEqual(toto, True)


  def test_isValidHostName(self):
    bad_hostname = (
                    'thetuxhouse',
                    'thetuxhouse.',
                    'thetuxhouse.priv',
                    'thetuxhouse.priv.',
                    '.thetuxhouse.priv',
                    '.thetuxhouse.priv.',
                  )

    good_hostname = (
                    'toto.thetuxhouse.priv',
                    'toto.thetuxhouse.priv.',
                    'toto-1.thetuxhouse.priv.',
                    '1-toto-1.thetuxhouse.priv.',
                  )

    print "\n\tFALSE CASES :"
    for hostname in bad_hostname:
      try:
        toto = self.NS._isValidHostName(hostname)
      except NameError:
        print "\t%s" % (hostname)
        pass
      else:
        print "expected a NameError"

    print "\n\tTRUE CASES :"
    for hostname in good_hostname:
      toto = self.NS._isValidHostName(hostname)
      print "\t%s\t%s" % (hostname, toto)
      self.assertEqual(toto, True)


  def test_validateRecord(self):

    print "\n\tTRUE CASES :"
    fqdn = 'toto.thetuxhouse.priv'
    record_type = 'A'
    ip_address = '192.168.0.200'
    toto = self.NS._validateRecord(fqdn, record_type, ip_address)
    print "\t%s\t%s\t%s\t%s" % (fqdn, record_type, ip_address, toto)
    self.assertEqual(toto, True)

    print "\n\tFALSE CASES :"
    fqdn = 'thetuxhouse.priv.'
    record_type = 'A'
    ip_address = '192.168.0.200'
    try:
      toto = self.NS._validateRecord(fqdn, record_type, ip_address)
    except NameError:
      print "\t%s\t%s\t%s" % (fqdn, record_type, ip_address)
      pass
    else:
      print "expected a NameError"

    fqdn = 'toto.thetuxhouse.priv'
    record_type = 'B'
    ip_address = '192.168.0.200'
    try:
      toto = self.NS._validateRecord(fqdn, record_type, ip_address)
    except NameError:
      print "\t%s\t%s\t%s" % (fqdn, record_type, ip_address)
      pass
    else:
      print "expected a NameError"

    fqdn = 'toto.thetuxhouse.priv'
    record_type = 'A'
    ip_address = '192.168.0.'
    try:
      toto = self.NS._validateRecord(fqdn, record_type, ip_address)
    except NameError:
      print "\t%s\t%s\t%s" % (fqdn, record_type, ip_address)
      pass
    else:
      print "expected a NameError"


suite = unittest.TestLoader().loadTestsFromTestCase(nsupdateTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)
