# -*- coding: utf-8 -*-
"""
pynsupdate.py

Copyright (C) 2012 Tchoum Tux <pitit.atchoum@free.fr>
This file is part of Phoreman-Agent
http://phoreman-agent.thetuxhouse.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import logging
import re
import socket
import subprocess

# depends on python-dnspython
import dns.resolver
import dns.reversename


class nsupdate:
  """
  Wrapper class for the nsupdate commmand
  """

  def __init__(self, config):
    """
    Init object nsupdate
    @param config:
    @type config: array
    """
    # Init logger
    self.logger = logging.getLogger()

    # Init nsupdate console
    self.nsupdate_command = "%s -v -k %s" % (config['nsupdate_cmd'], config['key_file'])

    # Init connection buffer
    self.batch_connect = []
    self.batch_connect.append("server %s" % config['server'])

    # Init buffer
    self.batch = []

    # Open dns console
    try:
      fd = open(config['nsupdate_cmd'], 'r')
    except IOError:
      self.logger.error("Cannot open Dns console '%s' !!" % config['nsupdate_cmd'])
      raise NameError("Cannot open Dns console '%s' !!" % config['nsupdate_cmd'])
    fd.close()

    # Open Bind9 key file
    try:
      fd = open(config['key_file'], 'r')
    except IOError:
      self.logger.error("Cannot open Bind9 key file '%s' !!" % config['key_file'])
      raise NameError("Cannot open Bind9 key file '%s' !!" % config['key_file'])
    fd.close()


  def existsDnsRecord(self, fqdn):
    """
    Check dns record existence
    @param fqdn:
    @type fqdn: string
    @return: True if exists, False if not exists
    @rtype: bool
    """
    if self._isValidHostName(fqdn):
      try:
        answer = dns.resolver.query(fqdn, 'A')
      except dns.resolver.NXDOMAIN:
        self.logger.debug("existsDnsRecord : record '%s' does not exist" % fqdn)
        return False
      else:
        record = {}
        record['host_name'] = fqdn
        for data in answer:
          self.logger.debug("existsDnsRecord  : record '%s' has IPv4 address : '%s'" % (fqdn, data.address))
          record['ip_address'] = "%s" % data.address
        return record


  def existsHostRecord(self, ip_address):
    """
    Check host record existence
    @param ip_address:
    @type ip_address: string
    @return: True if exists, False if not exists
    @rtype: bool
    """
    if self._isValidIpv4(ip_address):
      try:
        reverse_name = dns.reversename.from_address(ip_address)
        answer = dns.resolver.query(reverse_name, 'PTR')
      except dns.resolver.NXDOMAIN:
        self.logger.debug("existsHostRecord : record '%s' does not exist" % ip_address)
        return False
      else:
        record = {}
        record['ip_address'] = ip_address
        for data in answer:
          self.logger.debug("existsHostRecord : record '%s' has hostname : '%s'" % (ip_address, data.target))
          record['host_name'] = "%s" % data.target
        return record


  def setRecord(self, fqdn, record_type, ip_address, ttl=10800):
    """
    Set record
    @param fqdn:
    @type fqdn: string
    @param record_type:
    @type record_type: string
    @param ip_address:
    @type ip_address: string
    @param ttl:
    @type ttl: int
    @return: True or raise an error
    @rtype: bool
    """
    if self._validateRecord(fqdn, record_type, ip_address):

      if record_type == 'A+PTR':
        if not self.existsDnsRecord(fqdn) and not self.existsHostRecord(ip_address):
          # add PTR record first to be sure we are in the good reverse zone
          if self._addRecord(fqdn, 'PTR', ip_address, ttl) == True:
            # then A record
            self._addRecord(fqdn, 'A', ip_address, ttl)
        else:
          self.logger.warning("setRecord : record '%s' / '%s' already exists" % (fqdn, ip_address))
          raise NameError("setRecord : record '%s' / '%s' already exists" % (fqdn, ip_address))

      elif record_type == 'A':
        if not self.existsDnsRecord(fqdn):
          self._addRecord(fqdn, 'A', ip_address, ttl)
        else:
          self.logger.warning("setRecord : record '%s' / '%s' already exists" % (fqdn, ip_address))
          raise NameError("setRecord : record '%s' / '%s' already exists" % (fqdn, ip_address))

      elif record_type == 'PTR':
        if not self.existsHostRecord(ip_address):
          self._addRecord(fqdn, 'PTR', ip_address, ttl)
        else:
          self.logger.warning("setRecord : record '%s' / '%s' already exists" % (fqdn, ip_address))
          raise NameError("setRecord : record '%s' / '%s' already exists" % (fqdn, ip_address))


  def _addRecord(self, fqdn, record_type, ip_address, ttl=10800):
    """
    Add record
    @param fqdn:
    @type fqdn: string
    @param record_type:
    @type record_type: string
    @param ip_address:
    @type ip_address: string
    @param ttl:
    @type ttl: int
    @return: True or raise an error
    @rtype: bool
    """
    record = self._formatRecord(fqdn, ip_address)
    if record_type == 'A':
      self.batch.append("zone %s" % record['zone'])
      self.batch.append("update add %s %s IN A %s" % (record['fqdn'], ttl, ip_address))
      self.batch.append("send")
      if self._commitCommand() == True:
        self.logger.info("Added 'A' record '%s' with ip address '%s'" % (record['fqdn'], ip_address))
        return True
    elif record_type == 'PTR':
      self.batch.append("zone %s" % record['reverse_zone'])
      self.batch.append("update add %s %s IN PTR %s" % (record['reverse_ip'], ttl, record['fqdn']))
      self.batch.append("send")
      if self._commitCommand() == True:
        self.logger.info("Added 'PTR' record '%s' with ip address '%s'" % (record['reverse_ip'], record['fqdn']))
        return True


  def unsetRecord(self, fqdn, record_type, ip_address):
    """
    Unset record
    @param fqdn:
    @type fqdn: string
    @param record_type:
    @type record_type: string
    @param ip_address:
    @type ip_address: string
    @return: True or raise an error
    @rtype: bool
    """
    if self._validateRecord(fqdn, record_type, ip_address):

      if record_type == 'A+PTR':
        if self.existsDnsRecord(fqdn) and self.existsHostRecord(ip_address):
          # delete PTR record first to be sure we are in the good reverse zone
          if self._deleteRecord(fqdn, 'PTR', ip_address) == True:
            # then A record
            self._deleteRecord(fqdn, 'A', ip_address)
        else:
          self.logger.warning("unsetRecord : record '%s' / '%s' does not exist" % (fqdn, ip_address))
          raise NameError("unsetRecord : record '%s' / '%s' does not exist" % (fqdn, ip_address))

      elif record_type == 'A':
        if self.existsDnsRecord(fqdn):
          self._deleteRecord(fqdn, record_type, ip_address)
        else:
          self.logger.warning("unsetRecord : record '%s' / '%s' does not exist" % (fqdn, ip_address))
          raise NameError("unsetRecord : record '%s' / '%s' does not exist" % (fqdn, ip_address))

      elif record_type == 'PTR':
        if self.existsHostRecord(ip_address):
          self._deleteRecord(fqdn, 'PTR', ip_address)
        else:
          self.logger.warning("unsetRecord : record '%s' / '%s' does not exist" % (fqdn, ip_address))
          raise NameError("unsetRecord : record '%s' / '%s' does not exist" % (fqdn, ip_address))


  def _deleteRecord(self, fqdn, record_type, ip_address):
    """
    Delete record
    @param fqdn:
    @type fqdn: string
    @param record_type:
    @type record_type: string
    @param ip_address:
    @type ip_address: string
    @return: True or raise an error
    @rtype: bool
    """
    record = self._formatRecord(fqdn, ip_address)
    if record_type == 'A':
      self.batch.append("zone %s" % record['zone'])
      self.batch.append("update delete %s IN A %s" % (record['fqdn'], ip_address))
      self.batch.append("send")
      if self._commitCommand() == True:
        self.logger.info("Deleted 'A' record '%s' with ip address '%s'" % (record['fqdn'], ip_address))
        return True
    elif record_type == 'PTR':
      self.batch.append("zone %s" % record['reverse_zone'])
      self.batch.append("update delete %s IN PTR %s" % (record['reverse_ip'], record['fqdn']))
      self.batch.append("send")
      if self._commitCommand() == True:
        self.logger.info("Deleted 'PTR' record '%s' with ip address '%s'" % (record['reverse_ip'], record['fqdn']))
        return True


  def _commitCommand(self):
    """
    Commit action
    @return: True or raise an error
    @rtype: bool
    """
    nsupdate_console = subprocess.Popen(self.nsupdate_command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    self.logger.debug("--------------------")
    for line in self.batch_connect:
      self.logger.debug(line)
      nsupdate_console.stdin.write('%s\n' % line)
    for line in self.batch:
      self.logger.debug(line)
      nsupdate_console.stdin.write("%s\n" % line)
    self.logger.debug("--------------------")
    nsupdate_console.stdin.flush()
    output = nsupdate_console.communicate()
    if output[1] != '':
      self.batch = []
      output = output[1].split("\n")
      self.logger.error("%s" % output[0])
      raise NameError("%s" % output[0])
    elif (output[0] == '' and output[1] == ''):
      self.batch = []
      return True


  def _formatRecord(self, fqdn, ip_address):
    """
    Format record
    @param fqdn:
    @type fqdn: string
    @param ip_address:
    @type ip_address: string
    @rtype: dict
    """
    record = {}
    # extract domain name (dns zone) from fqdn
    fqdn_parts = fqdn.split('.')
    record['zone'] = '.'.join(fqdn_parts[1:])
    # make non-fqdn->fqdn
    if fqdn.endswith('.'):
      record['fqdn'] = fqdn
    else:
      record['fqdn'] = '%s.' % fqdn
    # split ip address to revert it
    ip_split = ip_address.split('.')
    record['reverse_ip'] = "%s.%s.%s.%s.in-addr.arpa" % (ip_split[3], ip_split[2], ip_split[1], ip_split[0])
    record['reverse_zone'] = "%s.%s.%s.in-addr.arpa" % (ip_split[2], ip_split[1], ip_split[0])
    return record


  def _validateRecord(self, fqdn, record_type, ip_address):
    """
    Validate record parameters
    @param fqdn:
    @type fqdn: string
    @param record_type:
    @type record_type: string
    @param ip_address:
    @type ip_address: string
    @return: True if valid
    @rtype: bool
    """
    if self._isValidHostName(fqdn):
      if self._isValidRecordType(record_type):
        if self._isValidIpv4(ip_address):
          return True
        else:
          return False
      else:
        return False
    else:
      return False


  def _isValidRecordType(self, record_type):
    """
    Validates DNS Record type
    @param record_type:
    @type record_type: string
    @return: True or raise an error
    @rtype: bool
    """
    record_type_list = ['A', 'PTR', 'A+PTR']
    if record_type in record_type_list:
      return True
    else:
      self.logger.error("Wrong DNS record type : '%s'" % record_type)
      raise NameError("Wrong DNS record type : '%s'" % record_type)


  def _isValidIpv4(self, ip_address):
    """
    Validates IPv4 address
    @param ip_address:
    @type ip_address: string
    @return: True or raise an error
    @rtype: bool
    """
    try:
      addr = socket.inet_pton(socket.AF_INET, ip_address)
    except AttributeError:
      try:
        addr = socket.inet_aton(ip_address)
      except socket.error:
        self.logger.error("Invalid IP address : '%s'" % ip_address)
        raise NameError("Invalid IP address : '%s'" % ip_address)
      return ip_address.count('.') == 3
    except socket.error:
      self.logger.error("Invalid IP address : '%s'" % ip_address)
      raise NameError("Invalid IP address : '%s'" % ip_address)
    return True


  def _isValidHostName(self, fqdn):
    """
    Validates FQDN
    @param fqdn:
    @type fqdn: string
    @return: True or raise an error
    @rtype: bool
    """
    try:
      fqdn = fqdn.encode('idna').lower()
    except UnicodeError:
      self.logger.error("Invalid IDNA FQDN : '%s'" % fqdn)
      raise NameError("Invalid IDNA FQDN : '%s'" % fqdn)

    is_valid_host = re.compile(r'^(?=.{4,255}$)([a-zA-Z0-9][a-zA-Z0-9-]{,61}[a-zA-Z0-9]\.)+[a-zA-Z0-9]{2,5}.$')

    if bool(is_valid_host.match(fqdn)) == True:
      fqdn_parts = fqdn.split('.')
      if len(fqdn_parts) <= 2 or fqdn_parts[2] == '':
        self.logger.error("No subdomain in FQDN : '%s'" % fqdn)
        raise NameError("No subdomain in FQDN : '%s'" % fqdn)
      else:
        return True
    else:
      self.logger.error("Invalid FQDN from regex : '%s'" % fqdn)
      raise NameError("Invalid FQDN from regex : '%s'" % fqdn)
