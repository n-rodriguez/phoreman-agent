#
# setup.py
#
# Copyright (C) 2012 Tchoum Tux <pitit.atchoum@free.fr>
# This file is part of Phoreman-Agent
# http://phoreman-agent.thetuxhouse.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from setuptools import setup, find_packages

VERSION = '0.1.1'

DESCRIPTION="""\
** Phoreman Agent **

Copyright (c) 2012 Tchoum Tux

This package contains the phoreman-agent providing a REST interface
for Bind9 and isc-dhcp-server

For more information, please go to http://phoreman-agent.thetuxhouse.org
"""

setup(
			name = 'phoreman-agent',
			version = VERSION,
			description = "The Phoreman Agent",
			long_description = DESCRIPTION,
			author = 'Tchoum Tux',
			author_email = 'pitit.atchoum@free.fr',
			url = 'http://phoreman-agent.thetuxhouse.org',
			keywords = 'webservice, dns, dhcp',
			platforms = 'Linux',
			classifiers = [
				'Development Status :: 4 - Beta',
				'Environment :: Console',
				'Environment :: No Input/Output (Daemon)',
				'Intended Audience :: Developers',
				'Intended Audience :: End Users/Desktop',
				'Intended Audience :: System Administrators',
				'Intended Audience :: Telecommunications Industry',
				'License :: OSI Approved :: GNU Affero General Public License v3',
				'Operating System :: POSIX :: Linux',
				'Natural Language :: English',
				'Programming Language :: Python',
				'Topic :: Internet',
				'Topic :: System :: Boot',
				'Topic :: System :: Installation/Setup',
				'Topic :: System :: Networking',
				'Topic :: System :: Operating System'
			],
			license = 'AGPLv3',
			packages = find_packages(exclude=['ez_setup', 'examples', 'tests']),
			include_package_data = True,
			zip_safe = False,
			install_requires = [
				"bottle",
				"dnspython",
				"argparse"
			],
			entry_points = """
			# -*- Entry points: -*-
			""",
			data_files = [
				('install/etc/init.d'                        , ['install/etc/init.d/phoreman-agent']),
				('install/etc/default'                       , ['install/etc/default/phoreman-agent']),
				('install/etc/phoreman-agent'                , ['install/etc/phoreman-agent/phoreman-agent.conf']),
				('install/usr/local/share/phoreman-agent/'   , ['install/scripts/nsupdate.php', 'install/scripts/omshell.php'])
			],
			scripts = [
				'install/bin/pyomshell',
				'install/bin/pynsupdate',
				'install/bin/phoreman-agent'
			],
		)
