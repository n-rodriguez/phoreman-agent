#!/bin/bash

PYENV_HOME=$WORKSPACE/.pyenv/

create_virtualenv() {
  echo "**> creating virtualenv"
  virtualenv $PYENV_HOME
  . $PYENV_HOME/bin/activate
  pip install --quiet nosexcover
  pip install --quiet pylint
}

if [ -d $PYENV_HOME ] ; then
  echo "**> virtualenv exists"
else
  create_virtualenv
fi

if [ "$1" == "reload-env" ] ; then
  echo "**> recreate virtualenv"
  rm -rf $PYENV_HOME
  create_virtualenv
fi

# where your setup.py lives
. $PYENV_HOME/bin/activate
pip install --quiet $WORKSPACE/

# run code checker
if [ -e $WORKSPACE/pylint.out ] ; then
  rm -f $WORKSPACE/pylint.out
fi
pylint --rcfile $WORKSPACE/pylint.cfg -f parseable install/bin/* | tee -a pylint.out
pylint --rcfile $WORKSPACE/pylint.cfg -f parseable libpyomshell/ | tee -a pylint.out
pylint --rcfile $WORKSPACE/pylint.cfg -f parseable libpynsupdate/ | tee -a pylint.out

# run tests
nosetests --with-xcoverage --with-xunit --cover-package=phoreman-agent --cover-erase
