# -*- coding: utf-8 -*-
"""
pyomshell.py

Copyright (C) 2012 Tchoum Tux <pitit.atchoum@free.fr>
This file is part of Phoreman-Agent
http://phoreman-agent.thetuxhouse.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import logging
import re
import socket
import subprocess
import struct


class omshell:
  """
  Wrapper class for the omshell commmand
  """

  def __init__(self, config):
    """
    Init object omshell
    @param config:
    @type config: array
    """
    # Init logger
    self.logger = logging.getLogger()

    # Init omshell console
    self.omshell_command = config['omshell_cmd']

    # Init connection buffer
    self.batch_connect = []
    self.batch_connect.append("server %s" % config['server'])
    self.batch_connect.append("port %s" % config['port'])
    self.batch_connect.append("key %s %s" % (config['key_name'], config['key_secret']))
    self.batch_connect.append("connect")

    # Init commands buffer
    self.batch = []

    # Open dhcp console
    try:
      fd = open(config['omshell_cmd'], 'r')
    except IOError:
      self.logger.error("Cannot open Dhcp console '%s' !!" % config['omshell_cmd'])
      raise NameError("Cannot open Dhcp console '%s' !!" % config['omshell_cmd'])
    fd.close()

    # Open dhcp config file
    try:
      fd = open(config['config_file'], 'r')
      self.config_file = [line.strip() for line in fd.readlines()]
    except IOError:
      self.logger.error("Cannot process Dhcp config file '%s' !!" % config['config_file'])
      raise NameError("Cannot process Dhcp config file '%s' !!" % config['config_file'])
    fd.close()

    # Open dhcp leases file
    try:
      fd = open(config['lease_file'], 'r')
      self.lease_file = [line.strip() for line in fd.readlines()]
    except IOError:
      self.logger.error("Cannot process Dhcp leases file '%s' !!" % config['lease_file'])
      raise NameError("Cannot process Dhcp leases file '%s' !!" % config['lease_file'])
    fd.close()


  def lookupByMac(self, mac_address):
    """
    Check global (leases + hosts) existence by MAC address
    @param mac_address:
    @type mac_address: string
    @return: a dictionnary containing host infos if exists, False if not exists
    @rtype: dict
    """
    if self._isValidMacAddress(mac_address):
      entries_list = self._getEntries()
      if entries_list != False:
        for key, value in entries_list.iteritems():
          mac = value['host_mac']
          ip = value['host_ip']
          host_type = value['host_type']
          if mac_address == mac:
            entry = {}
            entry['host_name'] = key
            entry['host_ip'] = ip
            entry['host_mac'] = mac
            entry['host_type'] = host_type
            self.logger.debug("MAC address found : '%s'" % (entry))
            return entry
        self.logger.debug("MAC address not found : '%s'" % mac_address)
        return False
      else:
        return False


  def lookupByIp(self, ip_address):
    """
    Check global (leases + hosts) existence by IP address
    @param ip_address:
    @type ip_address: string
    @return: a dictionnary containing host infos if exists, False if not exists
    @rtype: dict
    """
    if self._isValidIpv4(ip_address):
      entries_list = self._getEntries()
      if entries_list != False:
        for key, value in entries_list.iteritems():
          mac = value['host_mac']
          ip = value['host_ip']
          host_type = value['host_type']
          if ip_address == ip:
            entry = {}
            entry['host_name'] = key
            entry['host_ip'] = ip
            entry['host_mac'] = mac
            entry['host_type'] = host_type
            self.logger.debug("IP address found  : '%s'" % (entry))
            return entry
        self.logger.debug("IP address not found  : '%s'" % ip_address)
        return False
      else:
        return False


  def lookupByHostName(self, host_name):
    """
    Check global (lease + host) existence by host name
    @param host_name:
    @type host_name: string
    @return: a dictionnary containing host infos if exists, False if not exists
    @rtype: dict
    """
    if self._isValidHostName(host_name):
      entries_list = self._getEntries()
      if entries_list != False:
        if host_name in entries_list:
          entry = {}
          entry['host_name'] = host_name
          entry.update(entries_list.get(host_name))
          self.logger.debug("Host name found   : '%s'" % (entry))
          return entry
        else:
          self.logger.debug("Host name not found   : '%s'" % host_name)
          return False
      else:
        return False


  def addHostEntry(self, ip_address, mac_address, host_name):
    """
    Add host entry
    @param ip_address:
    @type ip_address: string
    @param mac_address:
    @type mac_address: string
    @param host_name:
    @type host_name: string
    @return: True or raise an error
    @rtype: bool
    """
    if self._validateEntry(ip_address, mac_address, host_name):
      if not self.lookupByIp(ip_address):
        if not self.lookupByMac(mac_address):
          if not self.lookupByHostName(host_name):
            self.batch.append("new host")
            self.batch.append("set hardware-type = 1")
            self.batch.append("set hardware-address = %s" % mac_address)
            self.batch.append("set ip-address = %s" % ip_address)
            self.batch.append("set name = \"%s\"" % host_name)
            self.batch.append("create")
            if self._commitCommand() == True:
              self.logger.info("Added   '%s' record with IP address '%s' and MAC address '%s'" % (host_name, ip_address, mac_address))
              return True
          else:
            self.logger.warning("addHostEntry : Host name '%s' already in use" % host_name)
            raise NameError("addHostEntry : Host name '%s' already in use" % host_name)
        else:
          self.logger.warning("addHostEntry : MAC address '%s' already in use" % mac_address)
          raise NameError("addHostEntry : MAC address '%s' already in use" % mac_address)
      else:
        self.logger.warning("addHostEntry : IP address '%s' already in use" % ip_address)
        raise NameError("addHostEntry : IP address '%s' already in use" % ip_address)


  def delHostEntry(self, ip_address, mac_address, host_name):
    """
    Delete host entry
    @param ip_address:
    @type ip_address: string
    @param mac_address:
    @type mac_address: string
    @param host_name:
    @type host_name: string
    @return: True or raise an error
    @rtype: bool
    """
    if self._validateEntry(ip_address, mac_address, host_name):
      if self.lookupByIp(ip_address):
        if self.lookupByMac(mac_address):
          if self.lookupByHostName(host_name):
            ip_split = ip_address.split('.')
            ip_hexa = "%x:%x:%x:%x" % (int(ip_split[0]), int(ip_split[1]), int(ip_split[2]), int(ip_split[3]))
            self.batch.append("new host")
            self.batch.append("set hardware-type = 00:00:00:01")
            self.batch.append("set hardware-address = %s" % mac_address)
            self.batch.append("set ip-address = %s" % ip_hexa)
            self.batch.append("set name = \"%s\"" % host_name)
            self.batch.append("open")
            self.batch.append("remove")
            if self._commitCommand() == True:
              self.logger.info("Deleted '%s' record with IP address '%s' and MAC address '%s'" % (host_name, ip_address, mac_address))
              return True
          else:
            self.logger.warning("delHostEntry : Host name '%s' not found" % host_name)
            raise NameError("delHostEntry : Host name '%s' not found" % host_name)
        else:
          self.logger.warning("delHostEntry : MAC address '%s' not found" % mac_address)
          raise NameError("delHostEntry : MAC address '%s' not found" % mac_address)
      else:
        self.logger.warning("delHostEntry : IP address '%s' not found" % ip_address)
        raise NameError("delHostEntry : IP address '%s' not found" % ip_address)


  def getSubnets(self):
    """
    Get subnets
    @return: a dictionnary of subnets and their options
    @rtype: dict
    """
    subnets_list = self._getEntriesByType('subnet', self.config_file)
    if (len(subnets_list) == 0):
      self.logger.error("getSubnets : no subnet declared in dhcpd.conf!!")
      raise NameError("getSubnets : no subnet declared in dhcpd.conf!!")
    else:
      self.logger.debug("getSubnets : found subnets in dhcpd.conf : %s" % subnets_list)
      return subnets_list


  def getSubnetFreeAddress(self, subnet_address, subnet_class):
    """
    Get free address in a subnet
    @param subnet_address:
    @type subnet_address: string
    @param subnet_class:
    @type subnet_class: int
    @return: a list of free ip address in a subnet or False
    @rtype: list
    """
    if self._isValidIpv4(subnet_address):
      if self._isValidNetmask(subnet_class):
        if self._subnetExists(subnet_address):
          ip_list = []

          # get subnet range
          subnet_properties = self._getSubnetProperties(subnet_address)
          range_start = subnet_properties['subnet_range_start'].split('.')
          range_end = subnet_properties['subnet_range_end'].split('.')

          # get reserved address
          reserved_address = self.getSubnetReservedAddress(subnet_address, subnet_class)
          if reserved_address != False:
            # make of list of reserved address
            reserved_address_list = []
            for key, value in reserved_address.iteritems():
              reserved_address_list.append(value['host_ip'])

            # generate a list of ip in a range
            for end_ip in range(int(range_start[3]), int(range_end[3])+1):
              ip_address = "%s.%s.%s.%s" % (range_start[0], range_start[1], range_start[2], end_ip)
              if ip_address not in reserved_address_list:
                ip_list.append("%s" % ip_address)

            if len(ip_list) > 0:
              self.logger.debug("getSubnetFreeAddress : found free address in subnet '%s' : %s" % (subnet_address, ip_list))
              return ip_list
            else:
              self.logger.info("getSubnetFreeAddress : no free address in subnet '%s'" % (subnet_address))
              return False

          else:
            for end_ip in range(int(range_start[3]), int(range_end[3])+1):
              ip_address = "%s.%s.%s.%s" % (range_start[0], range_start[1], range_start[2], end_ip)
              ip_list.append("%s" % ip_address)
            self.logger.debug("getSubnetFreeAddress : found free address in subnet '%s' : %s" % (subnet_address, ip_list))
            return ip_list
        else:
          self.logger.warning("getSubnetFreeAddress : subnet '%s/%s' not found" % (subnet_address, subnet_class))
          raise NameError("getSubnetFreeAddress : subnet '%s/%s' not found" % (subnet_address, subnet_class))


  def getSubnetReservedAddress(self, subnet_address, subnet_class):
    """
    Get reserved address in a subnet
    @param subnet_address:
    @type subnet_address: string
    @param subnet_class:
    @type subnet_class: int
    @return: a list of reserved ip address in a subnet or False
    @rtype: list
    """
    if self._isValidIpv4(subnet_address):
      if self._isValidNetmask(subnet_class):
        if self._subnetExists(subnet_address):
          entries_list = self._getEntries()
          if entries_list != False:
            reserved_address = self._compareIpToSubnet(entries_list, subnet_address, subnet_class)
            self.logger.debug("getSubnetReservedAddress : found reserved address in subnet '%s' : %s" % (subnet_address, reserved_address))
            return reserved_address
          else:
            self.logger.info("getSubnetReservedAddress : no reserved address in subnet '%s'" % (subnet_address))
            return False
        else:
          self.logger.warning("getSubnetReservedAddress : subnet '%s/%s' not found" % (subnet_address, subnet_class))
          raise NameError("getSubnetReservedAddress : subnet '%s/%s' not found" % (subnet_address, subnet_class))


  def _commitCommand(self):
    """
    Commit action
    @return: True or raise an error
    @rtype: bool
    """
    omshell_console = subprocess.Popen(self.omshell_command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    self.logger.debug("---------- omshell console ----------")
    for line in self.batch_connect:
      self.logger.debug(line)
      omshell_console.stdin.write('%s\n' % line)
    for line in self.batch:
      self.logger.debug(line)
      omshell_console.stdin.write('%s\n' % line)
    self.logger.debug("---------- omshell console ----------")
    omshell_console.stdin.flush()
    output = omshell_console.communicate()
    if output[1] != '':
      self.batch = []
      self.logger.error("%s" % output[1])
      raise NameError("%s" % output[1])
    else:
      output = output[0].split("\n")
      if "> can't destroy object: not found" in output:
        self.batch = []
        self.logger.error("Object not found")
        self.logger.debug("%s " % output)
        raise NameError("Object not found")
      elif "> can't open object: already exists" in output:
        self.batch = []
        self.logger.error("Object already exists")
        self.logger.debug("%s " % output)
        raise NameError("Object already exists")
      else:
        self.batch = []
        return True


  def _getEntries(self):
    """
    Get all entries (leases + hosts) in dhcpd.leases file
    @return: a dictionnary of subnets and their options
    @rtype: dict
    """
    new_list = {}
    host_list = self._getEntriesByType('host', self.lease_file)
    lease_list = self._getEntriesByType('lease', self.lease_file)
    if (len(host_list) == 0 and len(lease_list) == 0):
      # on ne raise pas une erreur car le fichier dhcpd.lease
      # peut etre vide (1er demarrage)
      self.logger.info("No entries found in dhcpd.leases file (empty file?)")
      return False
    else:
      new_list.update(host_list)
      new_list.update(lease_list)
      return new_list


  def _getEntriesByType(self, entries_type, file_to_parse):
    """
    Parse file for entries
    @param entries_type:
    @type entries_type: string
    @param file_to_parse:
    @type file_to_parse: string
    @return: a dictionnary of subnets|hosts|leases and their options
    @rtype: dict
    """
    regex = ''
    if entries_type == 'host':
      regex = re.compile('^host\s+(\S+)\s*\{$')
    elif entries_type == 'lease':
      regex = re.compile('^lease\s+(\S+)\s*\{$')
    elif entries_type == 'subnet':
      regex = re.compile('^subnet\s+(\S+)\s+netmask\s+(\S+)\s*\{$')

    # Setup markers
    entries = {}
    start_matched = 0
    end_matched = 0
    # Enumerate over config to keep track of position
    for pos, line in enumerate(file_to_parse):
      # See if our line matches "host"
      entry = regex.match(line)
      if entry:
        # Matched host entry, set our position and move on
        start_matched = pos
        continue
      # We have a position set for host entry, look for the end of its block
      if start_matched != 0 and line == '}':
        # Got the end of the block
        end_matched = pos
        # Extract the name of the host entry
        entry = regex.match(file_to_parse[start_matched])
        # Setup list of host
        entries[entry.group(1)] = []
        # Get host options between the start and end of the block
        # Also remove needless characters, only need the host info
        options = self._parseRecordOptions(file_to_parse[start_matched + 1:end_matched], entries_type)
        entries[entry.group(1)] = options
        # Reset position markers and move on
        start_matched = 0
        end_matched = 0
        continue

    # Now we got the list entries we have to clean up deleted items
    # (only for deleted hosts actually)
    deleted_entries = []
    for key, value in entries.iteritems():
      if len(value) == 1:
        deleted_entries.append(key)

      if entries_type != 'subnet':
        if value['host_ip'] == '':
          deleted_entries.append(key)

    for entry in deleted_entries:
      del entries[entry]

    return entries


  def _parseRecordOptions(self, list_options, entries_type):
    """
    Get dhcpd options
    """
    if entries_type == 'subnet':
      options_list = {}
      for option in list_options:
        exists = ''
        exists_ip1 = ''
        exists_ip2 = ''

        ### DHCP CONFIG FILE ###

        # Get subnet range
        subnet_range = re.compile('^range\s+(\S+)\s+(\S+);$')
        exists = subnet_range.match(option)
        if exists:
          options_list['subnet_range_start'] = exists.group(1)
          options_list['subnet_range_end'] = exists.group(2)
          continue

        # Get subnet mask
        subnet_mask = re.compile('^option\s+subnet-mask\s+(\S+);$')
        exists = subnet_mask.match(option)
        if exists:
          options_list['subnet_mask'] = exists.group(1)
          continue

        # Get subnet router
        subnet_router = re.compile('^option\s+routers\s+(\S+);$')
        exists = subnet_router.match(option)
        if exists:
          options_list['subnet_router'] = exists.group(1)
          continue

        # Get subnet domain name
        subnet_domain_name = re.compile('^option\s+domain-name\s+"(\S+)";$')
        exists = subnet_domain_name.match(option)
        if exists:
          options_list['subnet_domain_name'] = exists.group(1)
          continue

        # Get subnet domain name server
        subnet_domain_name_server = re.compile('^option\s+domain-name-servers\s+(\S+);$')
        exists = subnet_domain_name_server.match(option)
        if exists:
          options_list['subnet_domain_name_server'] = exists.group(1)
          continue

        # Get subnet boot server
        subnet_boot_server = re.compile('^next-server\s+(\S+);$')
        exists = subnet_boot_server.match(option)
        if exists:
          options_list['subnet_boot_server'] = exists.group(1)
          continue

    else:
      options_list = {}
      if entries_type == 'lease':
        options_list['host_type'] = 'lease'
      else:
        options_list['host_type'] = 'host'

      for option in list_options:
        exists = ''
        exists_ip1 = ''
        exists_ip2 = ''

        ### DHCP LEASE FILE ###

        # Get mac address
        host_mac = re.compile('^hardware\s+ethernet\s+(\S+);$')
        exists = host_mac.match(option)
        if exists:
          options_list['host_mac'] = exists.group(1)
          continue

        # Get ip address
        host_ip1 = re.compile('^fixed-address\s+(\S+);$')
        host_ip2 = re.compile('^set\s+clIP\s+=\s+"(\S+)";$')
        exists_ip1 = host_ip1.match(option)
        exists_ip2 = host_ip2.match(option)
        if exists_ip1:
          options_list['host_ip'] = exists_ip1.group(1)
          continue
        elif exists_ip2:
          options_list['host_ip'] = exists_ip2.group(1)
          continue
        else:
          options_list['host_ip'] = ''
          continue

        # Get hostname
        host_name = re.compile('^client-hostname\s+"(\S+)";$')
        exists = host_name.match(option)
        if exists:
          options_list['host_name'] = exists.group(1)
          continue

        # Get vendor class
        host_vendor_class = re.compile('^set\s+vendorclass\s+=\s+"(\S+)";$')
        exists = host_vendor_class.match(option)
        if exists:
          options_list['host_vendor_class'] = exists.group(1)
          continue

        # Get hosts deleted
        host_deleted = re.compile('^(deleted);$')
        exists = host_deleted.match(option)
        if exists:
          options_list['host_deleted'] = exists.group(1)
          continue

    return options_list


  def _getSubnetProperties(self,  subnet_address):
    """
    Get subnet properties
    @param subnet_address:
    @type subnet_address: string
    @return: True or raise an error
    @rtype: dict
    """
    subnets_list = self.getSubnets()
    if subnet_address in subnets_list:
      return subnets_list.get(subnet_address)


  def _subnetExists(self, subnet_address):
    """
    Check if a subnet exists
    @param subnet_address:
    @type subnet_address: string
    @return: True if exists or False if not exists
    @rtype: bool
    """
    subnets_list = self.getSubnets()
    if subnet_address in subnets_list:
      return True
    else:
      return False


  def _compareIpToSubnet(self, entries_list, subnet_address, subnet_class):
    """
    Check if a dictionary of IPs belongs to a subnet
    @param entries_list:
    @type entries_list: dict
    @param subnet_address:
    @type subnet_address: string
    @param subnet_class:
    @type subnet_class: integer
    @return: a dictionary of matching IPs
    @rtype: dict
    """
    ip_list = {}
    for key, value in entries_list.iteritems():
      ip_address = value['host_ip']
      if self._addressInNetwork(ip_address, subnet_address, subnet_class):
        ip_list[key] = value
    return ip_list


  def _addressInNetwork(self, ip_address, subnet_address, subnet_class):
    """
    Check if on IP belongs to a Network
    @param ip_address:
    @type ip_address: string
    @param subnet_address:
    @type subnet_address: string
    @param subnet_class:
    @type subnet_class: integer
    """
    if ip_address != '':
      ip_address = struct.unpack('=L', socket.inet_aton(ip_address))[0]
      netmask = struct.unpack('=L', socket.inet_aton(self._calcDottedNetmask(int(subnet_class))))[0]
      network = struct.unpack('=L', socket.inet_aton(subnet_address))[0] & netmask
      return (ip_address & netmask) == (network & netmask)


  def _calcDottedNetmask(self, mask):
    """
    Convert CIDR to netmask
    @param mask:
    @type mask: integer
    @return: CIDR converted to dotted netmask
    @rtype: string
    """
    bits = 0
    for i in xrange(32 - mask, 32):
      bits |= (1 << i)
    return "%d.%d.%d.%d" % ((bits & 0xff000000) >> 24, (bits & 0xff0000) >> 16, (bits & 0xff00) >> 8 , (bits & 0xff))


  def _validateEntry(self, ip_address, mac_address, host_name):
    """
    Validate host parameters
    @param ip_address:
    @type ip_address: string
    @param mac_address:
    @type mac_address: string
    @param host_name:
    @type host_name: string
    @return: True if valid
    @rtype: bool
    """
    if self._isValidIpv4(ip_address):
      if self._isValidMacAddress(mac_address):
        if self._isValidHostName(host_name):
          return True


  def _isValidNetmask(self, netmask):
    """
    Validates Netmask
    @param netmask:
    @type netmask: integer
    @return: True or raise an error
    @rtype: bool
    """
    if (int(netmask) > 0 and int(netmask) < 32):
      return True
    else:
      self.logger.error("Invalid Netmask : '%s'" % netmask)
      raise NameError("Invalid Netmask : '%s'" % netmask)


  def _isValidMacAddress(self, mac_address):
    """
    Validates MAC address
    @param mac_address:
    @type mac_address: string
    @return: True or raise an error
    @rtype: bool
    """
    is_valid_mac_address = re.compile(r'^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$')
    if bool(is_valid_mac_address.match(mac_address)) == True:
      return True
    else:
      self.logger.error("Invalid MAC address : '%s'" % mac_address)
      raise NameError("Invalid MAC address : '%s'" % mac_address)


  def _isValidIpv4(self, ip_address):
    """
    Validates IPv4 address
    @param ip_address:
    @type ip_address: string
    @return: True or raise an error
    @rtype: bool
    """
    try:
      addr = socket.inet_pton(socket.AF_INET, ip_address)
    except AttributeError:
      try:
        addr = socket.inet_aton(ip_address)
      except socket.error:
        self.logger.error("Invalid IP address : '%s'" % ip_address)
        raise NameError("Invalid IP address : '%s'" % ip_address)
      return ip_address.count('.') == 3
    except socket.error:
      self.logger.error("Invalid IP address : '%s'" % ip_address)
      raise NameError("Invalid IP address : '%s'" % ip_address)
    return True


  def _isValidHostName(self, fqdn):
    """
    Validates FQDN
    @param fqdn:
    @type fqdn: string
    @return: True or raise an error
    @rtype: bool
    """
    try:
      fqdn = fqdn.encode('idna').lower()
    except UnicodeError:
      self.logger.error("Invalid IDNA FQDN : '%s'" % fqdn)
      raise NameError("Invalid IDNA FQDN : '%s'" % fqdn)

    is_valid_host = re.compile(r'^(?=.{4,255}$)([a-zA-Z0-9][a-zA-Z0-9-]{,61}[a-zA-Z0-9]\.)+[a-zA-Z0-9]{2,5}.$')

    if bool(is_valid_host.match(fqdn)) == True:
      fqdn_parts = fqdn.split('.')
      if len(fqdn_parts) <= 2 or fqdn_parts[2] == '':
        self.logger.error("No subdomain in FQDN : '%s'" % fqdn)
        raise NameError("No subdomain in FQDN : '%s'" % fqdn)
      else:
        return True
    else:
      self.logger.error("Invalid FQDN from regex : '%s'" % fqdn)
      raise NameError("Invalid FQDN from regex : '%s'" % fqdn)
