<?php

define("PHOREMAN_AGENT", "http://192.168.0.1:8090");

function callCurl ($url, $call_type='get') {
  $ch = curl_init($url);

  curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  if ($call_type == 'post') {
    curl_setopt($ch, CURLOPT_POST, true);
  } elseif ($call_type == 'delete') {
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
  }

  $result = curl_exec($ch);
  if ($result === false) {
    echo "<pre>";
    echo "Error : " . curl_error($ch) . "\n";
    echo "URL : $url \n";
    echo "</pre>";
    return False;
  } else {
    return $result;
  }
}

if (isset($_POST['service']) && $_POST['service'] != '') {
  switch ($_POST['service']) {
    case 'dhcp':
      if (isset($_POST['method']) && $_POST['method'] != '') {
        switch ($_POST['method']) {

          case 'get_subnets':
            $url = PHOREMAN_AGENT . "/dhcp/subnets";
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'get_subnet_free_address':
            $url = PHOREMAN_AGENT . "/dhcp/subnets/free/" . $_POST['subnet_address'] . "/" . $_POST['subnet_class'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'get_subnet_reserved_address':
            $url = PHOREMAN_AGENT . "/dhcp/subnets/reserved/" . $_POST['subnet_address'] . "/" . $_POST['subnet_class'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'get_ip_address':
            $url = PHOREMAN_AGENT . "/dhcp/byip/" . $_POST['ip_address'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'get_mac_address':
            $url = PHOREMAN_AGENT . "/dhcp/bymac/" . $_POST['mac_address'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'get_host_info':
            $url = PHOREMAN_AGENT . "/dhcp/byhost/" . $_POST['host_name'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'add':
            $url = PHOREMAN_AGENT . "/dhcp/" . $_POST['ip_address'] . "/" . $_POST['mac_address'] . "/" . $_POST['host_name'] ;
            $response = callCurl($url, 'post');
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'delete':
            $url = PHOREMAN_AGENT . "/dhcp/" . $_POST['ip_address'] . "/" . $_POST['mac_address'] . "/" . $_POST['host_name'] ;
            $response = callCurl($url, 'delete');
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;
        }
      }
    break;

    case 'dns':
      if (isset($_POST['method']) && $_POST['method'] != '') {
        switch ($_POST['method']) {

          case 'get_host_record':
            $url = PHOREMAN_AGENT . "/dns/host/" . $_POST['ip_address'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'get_dns_record':
            $url = PHOREMAN_AGENT . "/dns/domain/" . $_POST['host_name'] ;
            $response = callCurl($url);
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'add':
            $url = PHOREMAN_AGENT . "/dns/" . $_POST['host_name'] . "/" . $_POST['dns_record_type'] . "/" . $_POST['ip_address'] ;
            $response = callCurl($url, 'post');
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;

          case 'delete':
            $url = PHOREMAN_AGENT . "/dns/" . $_POST['host_name'] . "/" . $_POST['dns_record_type'] . "/" . $_POST['ip_address'] ;
            $response = callCurl($url, 'delete');
            if ($response != False) {
              $response_decoded = json_decode($response, true);
              echo "<pre>";
              var_dump ($response_decoded);
              echo "</pre>";
            }
          break;
        }
      }
    break;
  }
}

?>

<html>
  <head>
    <style type="text/css">

      html { background-color: #eee; font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif; font-size: 0.8em; }
      body { padding: 15px; margin: 15px; }
      pre { background-color: #fff; border: 1px solid #ddd; padding: 5px; font-size: 1.2em; }
      div.main { width:75%; margin:0 auto; }
      div.dhcp { background-color: #fff; float: left; width: 40%; margin-left: 10%; border: 1px solid #ddd; padding: 5px; }
      div.dns { background-color: #fff; margin-left: 55%; width: 30%; border: 1px solid #ddd; padding: 5px; }
      table { background-color: #fff; font-size: 0.9em; font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif; }
    </style>
  </head>
  <body>

    <div class="main">

      <div class="dhcp">

        <form method="POST">
          <table align="center">
            <tr>
              <th>Get Subnets List</th>
            </tr>
            <tr>
                <td align="center">
                  <input type="hidden" name="service" value="dhcp" />
                  <input type="hidden" name="method" value="get_subnets" />
                  <input type="submit" value="Submit" />
                </td>
            </tr>
          </table>
        </form>

        <br />
        <br />

        <form method="POST">
          <table align="center">
            <tr>
              <th colspan="2">Get Address List in Subnet</th>
            </tr>
            <tr>
              <td colspan="2"><input type="radio" name="method" value="get_subnet_free_address">Free Address</td>
            </tr>
            <tr>
              <td colspan="2"><input type="radio" name="method" value="get_subnet_reserved_address">Reserved Address</td>
            </tr>
            <tr>
                <td align="right">Subnet address : </td>
                <td>
                  <input type="text" name="subnet_address" size="15" maxlength="15" /> / <input type="text" name="subnet_class" size="2" maxlength="2" />
                  <input type="hidden" name="service" value="dhcp" />
                  <input type="submit" value="Submit" />
                </td>
            </tr>
          </table>
        </form>

        <br />
        <br />

        <table align="center">
          <tr>
            <th colspan="2">Search Host</th>
          </tr>

          <tr>
            <form method="POST">
              <td align="right">By IP Address :</td>
              <td>
                <input type="text" name="ip_address" size="15" maxlength="15" />
                <input type="hidden" name="service" value="dhcp" />
                <input type="hidden" name="method" value="get_ip_address" />
                <input type="submit" value="Submit" />
              </td>
            </form>
          </tr>

          <tr>
            <form method="POST">
              <td align="right">By MAC Address :</td>
              <td>
                <input type="text" name="mac_address" size="15" maxlength="17" />
                <input type="hidden" name="service" value="dhcp" />
                <input type="hidden" name="method" value="get_mac_address" />
                <input type="submit" value="Submit" />
              </td>
            </form>
          </tr>

          <tr>
            <form method="POST">
              <td align="right">By Host name :</td>
              <td>
                <input type="text" name="host_name" />
                <input type="hidden" name="service" value="dhcp" />
                <input type="hidden" name="method" value="get_host_info" />
                <input type="submit" value="Submit" />
              </td>
            </form>
          </tr>
        </table>

        <br />
        <br />

        <form method="POST">
          <table align="center">
            <tr>
              <th colspan="2">Add or Delete DHCP Record</th>
            </tr>
            <tr>
              <td align="right"><input type="radio" name="method" value="add">Add</td>
              <td align="right"><input type="radio" name="method" value="delete">Delete</td>
            </tr>
            <tr>
                <td align="right">Host name :</td>
                <td><input type="text" name="host_name" /></td>
            </tr>
            <tr>
                <td align="right">IP Address :</td>
                <td><input type="text" name="ip_address" size="15" maxlength="15" /></td>
            </tr>
            <tr>
                <td align="right">MAC Address :</td>
                <td><input type="text" name="mac_address" size="15" maxlength="17" /><br /></td>
            </tr>
            <tr>
              <td colspan="2">
                <input type="hidden" name="service" value="dhcp" />
                <input type="submit" value="Submit" />
              </td>
            </tr>
          </table>
        </form>

      </div>

      <div class="dns">


          <table align="center">
            <tr>
              <th colspan="2">Search Host</th>
            </tr>
            <tr>
              <form method="POST">
                <td align="right">By IP Address :</td>
                <td>
                  <input type="text" name="ip_address" size="15" maxlength="15" />
                  <input type="hidden" name="service" value="dns" />
                  <input type="hidden" name="method" value="get_host_record" />
                  <input type="submit" value="Submit" />
                </td>
              </form>
            </tr>
            <tr>
              <form method="POST">
                <td align="right">By Host name :</td>
                <td>
                  <input type="text" name="host_name" />
                  <input type="hidden" name="service" value="dns" />
                  <input type="hidden" name="method" value="get_dns_record" />
                  <input type="submit" value="Submit" />
                </td>
              </form>
            </tr>
          </table>


        <br />
        <br />

        <form method="POST">
          <table align="center">
            <tr>
              <th colspan="2">Add or Delete DNS Record</th>
            </tr>
            <tr>
              <td align="right"><input type="radio" name="method" value="add">Add</td>
              <td align="right"><input type="radio" name="method" value="delete">Delete</td>
            </tr>
            <tr>
                <td align="right">Host name :</td>
                <td><input type="text" name="host_name" /></td>
            </tr>
            <tr>
                <td align="right">IP Address :</td>
                <td><input type="text" name="ip_address" size="15" maxlength="15" /></td>
            </tr>
            <tr>
                <td align="right">DNS Type :</td>
                <td>
                  <select name="dns_record_type">
                    <option value="A">A</option>
                    <option value="PTR">PTR</option>
                    <option value="A+PTR">A+PTR</option>
                  </select>
                </td>
            </tr>
            <tr>
              <td colspan="2">
                <input type="hidden" name="service" value="dns" />
                <input type="submit" value="Submit" />
              </td>
            </tr>
          </table>
        </form>
      </div>

    </div>

  </body>
</html>
